import React from "react";
import "./css/signup.css";
import dappLogo from "./img/dapplogo.png";
import back1 from "./img/back1.jpg";

function SignupForm() {
    return (
      <div className="SignupAll">
        {/* <img src={back1} id="backgroundimg1"/> */}
        <div className="headingSign">
            <img src={dappLogo} id="LogoSign"/>
            <a href="/login" id="LoginHead1">Login</a>
        </div>
        <h1 id="headS">Join Us</h1>
        <form id="signupForm">
            <label for="name">Name:</label><br/>
            <input type="text" name="name"/><br/>

            <label for="email">Email</label><br/>
            <input type="email" name="email"/><br/>

            <label for="waddress">Wallet Address</label><br/>
            <input type="text" name="waddress"/><br/>

            <label for="uname">Username</label><br/>
            <input type="text" name="uname"/><br/>

            <label for="pword">Password</label><br/>
            <input type="password" name="pword"/><br/>
            
            <label for="Cpword">Confirm Password</label><br/>
            <input type="password" name="Cpword"/><br/>
            
            <input type="submit" id="signSubmit" value="Submit"/>
            <p>Already have an account? <a href="/login">Login</a></p>
        </form>
        
      </div>
    );
  }
  
  export default SignupForm;