import React, { useState } from 'react';
import './css/user.css';
import Illustration from '../img/Illustration.png';
import B1 from '../img/44.jpg';
import NavbarD from './navbarD';

const UserDash = () => {

  const data1 = [
    { profilePic: B1, name: 'John Doe', email: 'john.doe@example.com' },
    { profilePic: Illustration, name: 'Jane Smith', email: 'jane.smith@example.com' },
    { profilePic: 'pic3.jpg', name: 'Alex Johnson', email: 'alex.johnson@example.com' },
  ];

  const userCount = data1.filter((item) => item).length;

  return (
    <div className="DashAll">
      <NavbarD />

      <div className="HeadingAd1">
        <h1>Hello Admin</h1>
        <p>It's good to see you again!</p>
      </div>

      <img src={Illustration} id="imgill1" />

      <div className="headingDD1" id="hd11">
        <h1>{userCount}</h1>
        <p>Total</p>
        <p>Users</p>
      </div>

      <h2 id="Ttitle21">User</h2>

      <table className="DashTable1" id="UserTable1">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          {data1.map((item, index) => (
            <tr key={index}>
              <td>
                <img src={item.profilePic} alt="Profile" style={{ width: '70px', height: '50px', borderRadius: '10px' }} />
              </td>
              <td>
                <h4>{item.name}</h4>
                <p>{item.email}</p>
              </td>
              <td> <input type='button' value="Action" /> </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default UserDash;
