import React, { useState } from 'react';
import './css/campaign.css';
import Illustration from '../img/Illustration.png';
import B1 from '../img/44.jpg';
import NavbarD from './navbarD';

const CampaignDash = () => {
  const allData = [
    { bannerpic: B1, title: 'Example Title 1', status: 'Active', address: '12212121' },
    { bannerpic: 'pic1.jpg', title: 'Example Title 2', status: 'Completed', address: '121edq12e31de' },
    { bannerpic: 'pic1.jpg', title: 'Example Title 3', status: 'Active', address: '13bhcjh3h1jh3v' },
  ];

  const [filteredData, setFilteredData] = useState(allData);
  const [activeCategory, setActiveCategory] = useState('All');

  const handleCategoryClick = (category) => {
    setActiveCategory(category);

    if (category === 'All') {
      setFilteredData(allData);
    } else {
      const filtered = allData.filter((item) => item.status === category);
      setFilteredData(filtered);
    }
  };

  const completedCount = allData.filter((item) => item.status === 'Completed').length;
  const activeCount = allData.filter((item) => item.status === 'Active').length;

  return (
    <div className="DashAll2">
      <NavbarD />

      <div className="HeadingAd2">
        <h1>Hello Admin</h1>
        <p>It's good to see you again!</p>
      </div>

      <img src={Illustration} id="imgill2" />

      <div className="headingDD2" id="hd12">
        <h1>{completedCount}</h1>
        <p>Campaigns</p>
        <p>Completed</p>
      </div>

      <div className="headingDD2" id="hd22">
        <strong>
          <h1>{activeCount}</h1>
        </strong>
        <p>Campaigns</p>
        <p>in progress</p>
      </div>

      <h2 id="Ttitle12">Campaigns</h2>
      <div className="categories2">
        <button
          className={activeCategory === 'All' ? 'active' : ''}
          onClick={() => handleCategoryClick('All')}
        >
          All
        </button>
        <button
          className={activeCategory === 'Completed' ? 'active' : ''}
          onClick={() => handleCategoryClick('Completed')}
        >
          Completed
        </button>
        <button
          className={activeCategory === 'Active' ? 'active' : ''}
          onClick={() => handleCategoryClick('Active')}
        >
          Active
        </button>
      </div>

      <table className="DashTable2" id="CampaignTable2">
        <thead>
          <tr>
            <th></th>
            <th>Title</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          {filteredData.map((item, index) => (
            <tr key={index}>
              <td>
                <img src={item.bannerpic} alt="Banner Img" style={{ width: '70px', height: '50px', borderRadius: '10px' }} />
              </td>
              <td>
                <h4>{item.title} </h4>
                <p>{item.address} </p>
              </td>
              <td>{item.status}</td>
              <td> <input type='button' value="View" /> </td>
            </tr>
          ))}
        </tbody>
      </table>

    </div>
  );
};

export default CampaignDash;
