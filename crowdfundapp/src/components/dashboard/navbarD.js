import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import "./css/navbarD.css";
import CF from "../img/CF..png";

import { FaHome, FaUser, FaEnvelope, FaCog, FaSignOutAlt } from 'react-icons/fa';

const NavbarD = () => {
  const [showConfirmation, setShowConfirmation] = useState(false);
  const navigate = useNavigate();

  const navigateTo = (route) => {
    navigate(route);
  };

  const handleLogout = () => {
    setShowConfirmation(true);
  };

  const handleConfirmLogout = () => {
    // Perform logout logic here, such as clearing session or token
    // You can use an API call or local storage manipulation, depending on your setup
    navigateTo('/login');
  };

  const handleCancelLogout = () => {
    setShowConfirmation(false);
  };

  return (
    <div className='navbarD'>
        <nav>
            <ul>
                <img src={CF} id='CFlogo'/>
                <li>
                    <button onClick={() => navigateTo('/dashboard')}>
                        <FaHome className='react_icon' />
                    </button>
                </li>
                <li>
                    <button onClick={() => navigateTo('/user')}>
                        <FaUser className='react_icon'/>
                    </button>
                </li>
                <li>
                    <button onClick={() => navigateTo('/campaign')}>
                        <FaEnvelope className='react_icon'/>
                    </button>
                </li>
                <li>
                    <button onClick={() => navigateTo('/settings')}>
                        <FaCog className='react_icon'/>
                    </button>
                </li>
                <li>
                    <button onClick={handleLogout}>
                        <FaSignOutAlt className='react_icon'/>
                    </button>
                </li>
            </ul>
        </nav>
        {showConfirmation && (
          <div className="confirmation-dialog">
            <p>Are you sure you want to log out?</p>
            <div className="button-container">
              <button onClick={handleConfirmLogout}>Yes</button>
              <button onClick={handleCancelLogout}>No</button>
            </div>
          </div>
        )}
    </div>
  );
};

export default NavbarD;
