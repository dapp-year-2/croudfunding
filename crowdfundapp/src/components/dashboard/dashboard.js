import React, { useState } from 'react';
import './css/dashboard.css';
import Illustration from '../img/Illustration.png';
import B1 from '../img/44.jpg';
import NavbarD from './navbarD';

const Dashboard = () => {
  const allData = [
    { bannerpic: B1, title: 'Example Title 1', status: 'Active', address: '12212121' },
    { bannerpic: 'pic1.jpg', title: 'Example Title 2', status: 'Completed', address: '121edq12e31de' },
    { bannerpic: 'pic1.jpg', title: 'Example Title 3', status: 'Active', address: '13bhcjh3h1jh3v' },
  ];

  const [filteredData, setFilteredData] = useState(allData);
  const [activeCategory, setActiveCategory] = useState('All');

  const handleCategoryClick = (category) => {
    setActiveCategory(category);

    if (category === 'All') {
      setFilteredData(allData);
    } else {
      const filtered = allData.filter((item) => item.status === category);
      setFilteredData(filtered);
    }
  };

  const data1 = [
    { profilePic: B1, name: 'John Doe', email: 'john.doe@example.com' },
    { profilePic: Illustration, name: 'Jane Smith', email: 'jane.smith@example.com' },
    { profilePic: 'pic3.jpg', name: 'Alex Johnson', email: 'alex.johnson@example.com' },
  ];

  const completedCount = allData.filter((item) => item.status === 'Completed').length;
  const activeCount = allData.filter((item) => item.status === 'Active').length;
  const userCount = data1.filter((item) => item).length;

  return (
    <div className="DashAll">
      <NavbarD />

      <div className="HeadingAd">
        <h1>Hello Admin</h1>
        <p>It's good to see you again!</p>
      </div>

      <img src={Illustration} id="imgill" />

      <div className="headingDD" id="hd1">
        <h1>{completedCount}</h1>
        <p>Campaigns</p>
        <p>Completed</p>
      </div>

      <div className="headingDD" id="hd2">
        <strong>
          <h1>{activeCount}</h1>
        </strong>
        <p>Campaigns</p>
        <p>in progress</p>
      </div>

      <div className="headingDD" id="hd3">
        <strong>
          <h1>{userCount}</h1>
        </strong>
        <p>Total</p>
        <p>User</p>
      </div>

      <h2 id="Ttitle1">Campaigns</h2>
      <div className="categories">
        <button
          className={activeCategory === 'All' ? 'active' : ''}
          onClick={() => handleCategoryClick('All')}
        >
          All
        </button>
        <button
          className={activeCategory === 'Completed' ? 'active' : ''}
          onClick={() => handleCategoryClick('Completed')}
        >
          Completed
        </button>
        <button
          className={activeCategory === 'Active' ? 'active' : ''}
          onClick={() => handleCategoryClick('Active')}
        >
          Active
        </button>
      </div>

      <table className="DashTable" id="CampaignTable">
        <thead>
          <tr>
            <th></th>
            <th>Title</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          {filteredData.map((item, index) => (
            <tr key={index}>
              <td>
                <img src={item.bannerpic} alt="Banner Img" style={{ width: '70px', height: '50px', borderRadius: '10px' }} />
              </td>
              <td>
                <h4>{item.title} </h4>
                <p>{item.address} </p>
              </td>
              <td>{item.status}</td>
              <td> <input type='button' value="View" /> </td>
            </tr>
          ))}
        </tbody>
      </table>

      <h2 id="Ttitle2">User</h2>

      <table className="DashTable" id="UserTable">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          {data1.map((item, index) => (
            <tr key={index}>
              <td>
                <img src={item.profilePic} alt="Profile" style={{ width: '70px', height: '50px', borderRadius: '10px' }} />
              </td>
              <td>
                <h4>{item.name}</h4>
                <p>{item.email}</p>
              </td>
              <td> <input type='button' value="Action" /> </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Dashboard;
