
import React from 'react';

import {Navbar,Container} from 'react-bootstrap'
import whiteLogo from "./../assets/img/logo.png";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import "./../assets/css/nav.css";
function LandingNavBar() {
  return (
        <>
            <Navbar bg="white" expand="lg">
                <Container>
                    <Navbar.Brand to="/">
                        <img
                            src={whiteLogo}
                            height="40"
                            className="d-inline-block align-top logo"
                            alt="React Bootstrap logo"
                        />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <div className="buttons">
                        <Link to="/login"><Button className="button" variant="primary">Login</Button></Link>
                        <Link to="/signup"><Button className="button" variant="outline-primary">Sign Up</Button></Link>
                    </div>
                </Container>
            </Navbar>
        </>
    

  );
}

export default LandingNavBar;