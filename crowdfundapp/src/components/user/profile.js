
import React from 'react';

import { Form, Button, Row, Col } from 'react-bootstrap';
import profile from './../assets/img/profile.png'
import './../assets/css/profile.css'

function Profile() {
  return (
    <div className='bg-white '>
        <h3>Profile</h3>
        <Row className="justify-content-center">
        <Col xs="auto " className='image'>
          <img src= {profile} alt="profile " />
          <input type='file' />
        </Col>
      </Row>
        <div className='profile_form form_border bg-white'>
            <Form>
                <Row className='mb-3'>
                    <Col md={12}>
                        <Form.Group>
                        <Form.Control type="text" placeholder="Name" />
                        </Form.Group>
                    </Col>
             
                </Row>
                <Row className='mb-3'>
                    <Col md={12}>
                        <Form.Group>
                        <Form.Control type="text" placeholder="Email" />
                        </Form.Group>
                    </Col>
                </Row>

                <Row className='mb-3'>
                    <Col md={12}>
                        <Form.Group>
                        <Form.Control type="text" rows={3} placeholder="Password " />
                        </Form.Group>
                    </Col>
               
                </Row>
                <Row className='mb-3'>
                    <Col md={12}>
                        <Form.Group>
                        <Form.Control type="text" placeholder="Confirm Password" />
                        </Form.Group>
                    </Col>
                </Row>
                <Button className='update' variant="primary" type="submit">
                Update
                </Button>
                <Button className='logout' variant="primary" type="submit">
                Logout
                </Button>
            </Form>     
        </div>
    </div>   
  );
}
export default Profile;