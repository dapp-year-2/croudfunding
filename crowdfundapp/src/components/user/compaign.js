import React from "react";
import { Container, Form, Button, Col,Row} from "react-bootstrap";
import './../assets/css/compaign.css'
import image1 from '../assets/img/image1.png'
import image2 from '../assets/img/image2.png'


function Compaigns(){
    return(
        <>
        <section className="compaignpicture" id="home">
            <Container className="items">
                <h2>About ChainFund
                </h2>
                <h6>A Blockchain-based CrowdFunding Platform for Everyone</h6>
            </Container>   
        </section>
        <div className='bg-white '>
         <h3>Create a Compaign</h3>
            <div className='fundraise_form form_border bg-white'>
            <Form>
                <Row className='mb-5'>
                <Col md={12}>
                    <Form.Group>
                    <Form.Control type="text" placeholder=" Project Title" />
                    </Form.Group>
                </Col>
                </Row>
                <Row className='mb-5'>
                <Col md={12}>
                    <Form.Group>
                    <Form.Control as="textarea" rows={3} placeholder="Explain for what and why you want to fundraise " />
                    </Form.Group>
                </Col>
                </Row>
                <Row className='mb-5'>
                <Col md={12}>
                    <Form.Group>
                    <Form.Control type="number" placeholder="Fundraiser goal (ex. 1ETH)" />
                    </Form.Group>
                </Col>
                </Row >
                <Row className='mb-5'>
                <Col md={12}>
                    <Form.Group>
                    <Form.Control type="date" placeholder="Start date" />
                    </Form.Group>
                </Col>
                </Row >
                <Row className='mb-5'>
                <Col md={12}>
                    <Form.Group>
                    <Form.Control type="date" placeholder="End date" />
                    </Form.Group>
                </Col>
                </Row >
                <Row className='mb-5'>
                <Col md={12}>
                    <Form.Group>
                    <Form.Control type="text" placeholder="wallet address" />
                    </Form.Group>
                </Col>
                </Row >
                <Row className='mb-5'>
                <Col md={12}>
                    <Form.Group>
                    <Form.Label>Image</Form.Label>
                    <Form.Control type="file" accept="image/*" placeholder='Drag and drop files here or click to upload' />
                    </Form.Group>
                </Col>
                </Row>
                <Button variant="primary" type="submit">
                Create a compaign
                </Button>
            </Form>     
        </div>
        <div className="mycompaigns">
        <hr></hr>
        <h4>My Compaigns</h4>
        <div className="mycompaign">
            <div className="prj-item">
                    <img height={"140px"} width={"100%"} src={image1} alt="item-img" />
                        <div className="prj-desc">
                              <h5>Universal Smart Lamp for Work and Entertainment</h5>
                              <p>The Boring Lamp's modular, magnetic design makes it easy to install, disassemble, and replace. Unlike traditional lights, you can customize your Boring Lamp lighting setup easily.</p>
                              <div className="progres">
                                  <span>50%</span>
                                  <div className="p-bar"><div className="progres-bar"></div></div>
                                  <p><strong>0.001 ETH</strong> raised out of <strong>0.002 ETH</strong></p>
                              </div>
                          </div>
                      </div>
                      <div className="prj-item">
                          <img height={"140px"} width={"100%"} src={image2} alt="item-img" />
                          <div className="prj-desc">
                              <h5>FUELL Flluid: World's Longest Range E-bike</h5>
                              <p>We're excited to introduce our next generation of powerful and stylish e-bikes — with the longest range in the world, extreme performance, and the revolutionary automatic shifting Valeo Cyclee Mid Drive Unit that will forever change the way you ride.</p>
                              <div className="progres">
                                  <span>50%</span>
                                  <div className="p-bar"><div className="progres-bar"></div></div>
                                  <p><strong>0.001 ETH</strong> raised out of <strong>0.002 ETH</strong></p>
                              </div>
                          </div>
                      </div>

                </div>
        </div>
        </div>
        </>
        
    )
}export default Compaigns