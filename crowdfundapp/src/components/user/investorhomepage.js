import React from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { FaSearch } from 'react-icons/fa';
import image1 from "./../assets/img/image1.png";
import image2 from "./../assets/img/image2.png";
import image3 from "./../assets/img/image3.png";
import './../assets/css/investorpage.css';
import { Link } from "react-router-dom";

function Investpage() {
  return (
    <>
      <section className="homepicture" id="home">
        <Container className="items">
          <h2>About ChainFund</h2>
          <h6>A Blockchain-based CrowdFunding Platform for Everyone</h6>
        </Container>
      </section>

      <Container className="my-4">
        <Row>
          <Col md={{ span: 6, offset: 3 }}>
            <Form className="d-flex">
              <Form.Control
                type="search"
                placeholder="Search for campaigns"
                className="py-2 px-3"
                aria-label="Search"
              />
              <Button className="search-btn" variant="outline-primary">
                <FaSearch />
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>

      <section className="exploreSection">
        <Container>
          <h2>All Projects</h2>
          <Row>
            <Col md={6} lg={4}>
              <div className="prj-item">
                <img src={image1} alt="item-img" />
                <div className="prj-desc">
                  <h5>Universal Smart Lamp for Work and Entertainment</h5>
                  <p>The Boring Lamp's modular, magnetic design makes it easy to install, disassemble, and replace. Unlike traditional lights, you can customize your Boring Lamp lighting setup easily.</p>
                  <div className="progres">
                    <span>50%</span>
                    <div className="p-bar"><div className="progres-bar"></div></div>
                    <p><strong>0.001 ETH</strong> raised out of <strong>0.002 ETH</strong></p>
                  </div>
                </div>
              </div>
            </Col>
            <Col md={6} lg={4}>
              <div className="prj-item">
                <Link to='/projectdetails'>
                  <img src={image2} alt="item-img" />
                </Link>
                <div className="prj-desc">
                  <h5>FUELL Flluid: World's Longest Range E-bike</h5>
                  <p>We're excited to introduce our next generation of powerful and stylish e-bikes — with the longest range in the world, extreme performance, and the revolutionary automatic shifting Valeo Cyclee Mid Drive Unit that will forever change the way you ride.</p>
                  <div className="progres">
                    <span>50%</span>
                    <div className="p-bar"><div className="progres-bar"></div></div>
                    <p><strong>0.001 ETH</strong> raised out of <strong>0.002 ETH</strong></p>
                  </div>
                </div>
              </div>
            </Col>
            <Col md={6} lg={4}>
              <div className="prj-item">
                <img src={image3} alt="item-img" />
                <div className="prj-desc">
                  <h5>Earthquakes in Turkey and Syria</h5>
                  <p>Highly rated charities providing relief and recovery...</p>
                  <div className="progres">
                    <span>50%</span>
                    <div className="p-bar"><div className="progres-bar"></div></div>
                    <p><strong>0.001 ETH</strong> raised out of <strong>0.002 ETH</strong></p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  )
}

export default Investpage;


