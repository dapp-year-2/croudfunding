import React from "react";
import { Container } from "react-bootstrap";
import './../assets/css/invest.css'
import image from './../assets/img/project.png'

function InvestForm(){
    return(
        <>
           <section className="investpicture" id="home">
                <Container className="items">
                    <h2>Help us bring our dream to life!
                    </h2>
                    <h6>Let's build something amazing together!</h6>
                </Container>   
            </section>
            <div className="invest">
                <h5>Invest</h5>
                <div className="invest-items border border-dark">
                    <h4><b>FUELL Flluid: World's Longest Range E-bike
                    </b></h4>
                    <img src={image} alt="" ></img>
                    <div>
                    <label>
                        <input type="checkbox" />
                        I have read and fully agree the terms and condition to invest in this project!
                    </label>
                    </div>
                    <div>
                        <button>Invest</button>
                    </div>
                </div>
            </div>
        </>
    )
}export default InvestForm;