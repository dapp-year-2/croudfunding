import React from "react";
import { Link } from "react-router-dom";
import details from './../assets/img/project.png'
import image1 from './../assets/img/image1.png'
import image2 from './../assets/img/image2.png'
import './../assets/css/projectdetails.css'

function ProjectDetails(){
    return(
        <>
        <div className="prj-detail">
            <div className="prj-details">
                <img src={details} alt="" ></img>
            </div>
            <div className="prj-details">
                <h4>
                    FUELL Flluid: World's Longest Range E-bike
                </h4>
                <p>
                We're excited to introduce our next generation of powerful and stylish e-bikes with the longest range in the world, extreme performance, and the revolutionary automatic shifting Valeo Cyclee Mid Drive Unit that will forever change the way you ride.
                </p>
                <div className="progres">
                    <span>50%</span>
                    <div className="p-bar"><div className="progres-bar"></div></div>
                    <p><strong>0.001 ETH</strong> raised out of <strong>0.002 ETH</strong></p>
                    <p>Fundraising started: 01/01/2023</p>
                    <p>Fundraising ends: 01/05/2023</p>

                </div>
            </div>
        </div>
        <hr></hr>
        <div className="prj-discrp">
            <div className="discrp">
                <h4>Project Description</h4>
                <p>FUELL Flluid is more than just an e-bike. It’s a revolution in transportation that combines 
                    high performance with style and sustainability. Under the guidance of its CTO Erik Buell,
                     the FUELL team has created a new way to move around cities and embrace electric two-wheelers.
                      Our first e-bike, the FUELL Flluid-1, was an instant hit with 4000+ sales worldwide. 
                      And now, we’re excited to introduce our next generation of powerful and stylish e-bikes — 
                      with the longest range in the world, extreme performance, and the revolutionary automatic 
                      shifting Valeo Cyclee Mid Drive Unit that will forever change the way you ride. <br></br> <br></br>

                    Flluid e-bikes are engineered under the guidance of the legendary Erik Buell, 
                    who has made significant contributions in the world of two-wheeled vehicles.
                     As founder of the Buell Motorcycle Company and former Harley-Davidson engineer, 
                     Erik Buell has created some of the most innovative and usable motorcycles. 
                     With the first-generation FUELL Flluid, he proved equally capable in creating
                      an e-bike that exceeds expectations. His unparalleled expertise in two-wheeler 
                      engineering allows for the Flluid range to shine in the saturated world of e-bikes.
                </p>
                <Link to='/investform'>
                    <button>
                        invest
                    </button>
                </Link>
             
            </div>
            <hr></hr>
        <div className="otherproject">
        <h4>Other Project</h4>
        <div className="otherprojects">
            <div className="prj-item">
                    <img height={"140px"} width={"100%"} src={image1} alt="item-img" />
                        <div className="prj-desc">
                              <h5>Universal Smart Lamp for Work and Entertainment</h5>
                              <p>The Boring Lamp's modular, magnetic design makes it easy to install, disassemble, and replace. Unlike traditional lights, you can customize your Boring Lamp lighting setup easily.</p>
                              <div className="progres">
                                  <span>50%</span>
                                  <div className="p-bar"><div className="progres-bar"></div></div>
                                  <p><strong>0.001 ETH</strong> raised out of <strong>0.002 ETH</strong></p>
                              </div>
                          </div>
                      </div>
                      <div className="prj-item">
                          <img height={"140px"} width={"100%"} src={image2} alt="item-img" />
                          <div className="prj-desc">
                              <h5>FUELL Flluid: World's Longest Range E-bike</h5>
                              <p>We're excited to introduce our next generation of powerful and stylish e-bikes — with the longest range in the world, extreme performance, and the revolutionary automatic shifting Valeo Cyclee Mid Drive Unit that will forever change the way you ride.</p>
                              <div className="progres">
                                  <span>50%</span>
                                  <div className="p-bar"><div className="progres-bar"></div></div>
                                  <p><strong>0.001 ETH</strong> raised out of <strong>0.002 ETH</strong></p>
                              </div>
                          </div>
                      </div>

                </div>
        </div>
        </div>
        </>
    )
}export default ProjectDetails;