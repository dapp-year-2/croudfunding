
import React from 'react';
import './components/user/LandingNavBar';
import Footer from './components/user/Footer';

import { BrowserRouter as Router,Routes,Route  } from 'react-router-dom';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
// import LandingPage from './components/landingpage';


import NavBar from './components/user/NavBar';
import InvestorPage from './components/user/investorhomepage';
import About from './components/user/about';
import Compaigns from './components/user/compaign';
import Profile from './components/user/profile';
import ProjectDetails from './components/user/profiledetails';
import InvestForm from './components/user/investform';

import Dashboard from './components/dashboard/dashboard';
import UserDash from './components/dashboard/user';
import CampaignDash from './components/dashboard/campaign';
import SettingDash from './components/dashboard/setting';
import SignupForm from './components/signup';
import LoginForm from './components/login';

function App() {
  return (
    <>
    {/* <InvestForm/> */}
    <Router>
      <NavBar/>
      <Routes>
        <Route path='/' element={<InvestorPage/>}/>
        <Route path='/compaign' element={<Compaigns/>}/>
        <Route path='/about' element={<About/>}/>
        <Route path='/myprofile' element={<Profile/>}/>
        <Route path='/projectdetails' element={<ProjectDetails/>}/>
        <Route path='/investform' element={<InvestForm/>}/>


        <Route path='/dashboard' element={<Dashboard/>}/>
        <Route path="/user" element={<UserDash />} />
        <Route path="/campaign" element={<CampaignDash />} />
        <Route path="/signup" element={<SignupForm/>} />
        <Route path="/login" element={<LoginForm/>} />
        <Route path="/settings" element={<SettingDash/>} />

      </Routes>
      <Footer/>
    </Router>
    </>

  );
}

export default App;
