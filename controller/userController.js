const User = require('./../model/userModel')




exports.getAllUser = async (req, res, next) => {
    try{
        const users = await User.find()
        res.status(200).json({data: users, status: 'success'})
    } catch(err){
        res.status(500).json({error: err.message});
    }
}


exports.createUser = async (req, res, next) => {
    try {
        
     const user = new User({
            name: req.body.name,
            email: req.body.email,
            walletAddress: req.body.walletAddress,
            username: req.body.username,
            password: req.body.password,
            passwordConfirm: req.body.passwordConfirm,

        });

        await user.save();

        res.json({ data: user, status: 'success' })
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}


exports.getUser = async (req, res,) => {
    try{
        const user = await User.findById(req.params.id);
        
        res.json({data:user, status: 'success'})
    } catch(err){
        res.status(500).json({error: err.message});
    }
}

exports.updateUser = async (req, res,) => {
    try{
        const user = await User.findByIdAndUpdate(req.params.id,req.body);
        
        res.json({data:user, status: 'success'})
    } catch(err){
        res.status(500).json({error: err.message});
    }
}

exports.deleteUser = async (req, res,) => {
    try{
        const user = await User.findByIdAndDelete(req.params.id);
        
        res.json({data:user, status: 'success'})
    } catch(err){
        res.status(500).json({error: err.message});
    }
}

